# Projet de compilation
Ce projet consiste à créer un compilateur qui traduit des fragments de JavaScript en un langage d'assemblage personnalisé.


## Pour compiler et executer le code :
il suffit de cloner le projet dans votre répertoire personnel avec la commande 
```
$ git clone https://gitlab.sorbonne-paris-nord.fr/12003572/comp.git
```
Ensuite afin de compiler et executer il vous suffit de taper les commandes suivantes :

```
$ cd /chemin/vers/le/fichier
$ make
$ ./main

```

## Membres :
 Chanez ARROUM
 Matthias bouquet

